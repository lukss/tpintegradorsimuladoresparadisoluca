using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemie2 : MonoBehaviour
{
    public GameObject EnemigoDividido1;
    public GameObject EnemigoDividido2;
    public ParticleSystem red;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    
      }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Espada") == true)
        {
            EnemigoDividido1.gameObject.SetActive(true);
            EnemigoDividido2.gameObject.SetActive(true);
            red.gameObject.SetActive(true);
        }

    }
}
