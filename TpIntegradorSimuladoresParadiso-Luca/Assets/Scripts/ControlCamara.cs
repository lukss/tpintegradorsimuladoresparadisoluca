﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
     public Transform lookAt;
      private bool smooth = true;
      private float smothSpeed = 0.125f;
      public Vector3 offset=new Vector3 (0, 0, -6.5f);
    public Transform[] views;
    public float transitionSpeed;
    Transform currentView;

    void Start()
    {
      //  currentView = transform;
 
    }
    

    private void LateUpdate()
    {
           Vector3 desiredPosition = lookAt.transform.position + offset;

           if (smooth)
           {
               transform.position = Vector3.Lerp(transform.position, desiredPosition, smothSpeed);
           }
           else
           {
               transform.position = desiredPosition;
           }
    /*  transform.position = Vector3.Lerp(transform.position, currentView.position, Time.deltaTime * transitionSpeed);
        Vector3 currentAngle = new Vector3(Mathf.Lerp(transform.rotation.eulerAngles.x, currentView.transform.rotation.eulerAngles.x, Time.deltaTime * transitionSpeed),
            Mathf.Lerp(transform.rotation.eulerAngles.x, currentView.transform.rotation.eulerAngles.y, Time.deltaTime * transitionSpeed),
            Mathf.Lerp(transform.rotation.eulerAngles.x, currentView.transform.rotation.eulerAngles.z, Time.deltaTime * transitionSpeed));*/


       // transform.eulerAngles = currentAngle;
    }

    
}