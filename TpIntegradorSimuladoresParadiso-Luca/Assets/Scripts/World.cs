using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class World : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
      Planets.singleton.agregarObjPlanets(GetComponent<Rigidbody>());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
