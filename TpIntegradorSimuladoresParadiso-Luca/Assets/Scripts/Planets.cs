using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planets : MonoBehaviour
{
    public List<Gravity2> Planetas;
    public static Planets singleton;
    // Start is called before the
    // frame update
    void Awake()
    {
        if (singleton == null)
        {
            singleton = this;

        }
        else
        {
            Destroy(gameObject);
        }
        
    }
    public void agregarObjPlanets(Rigidbody rObj)
    {
        foreach(Gravity2 planeta in Planetas)
        {
            planeta.addObjets(rObj);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
