using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class CiScript : MonoBehaviour
{

    [SerializeField] private CinemachineVirtualCamera virtualCamera;
    [SerializeField] private Camera cam;
    [SerializeField] private Camera cam2;
   
   
    // Start is called before the first frame update
    void Start()
    {
 
        virtualCamera.gameObject.SetActive(false);
        cam.gameObject.SetActive(false);
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        virtualCamera.gameObject.SetActive(true);
        cam.gameObject.SetActive(false);

        if (other.gameObject.CompareTag("Zone2") == true)
        {
           
            cam.gameObject.SetActive(false);
        }
       
    }

    private void OnTriggerExit(Collider other)
    {
        virtualCamera.gameObject.SetActive(false);
        cam.gameObject.SetActive(true);
        
        
    }

    
}
