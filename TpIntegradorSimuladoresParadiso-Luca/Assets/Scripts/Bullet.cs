﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public Rigidbody Rb;

    void Update()
    {

        Rb.AddForce(gameObject.transform.right * 1f, ForceMode.Impulse);

        Destroy(gameObject, 2f);

    }
}
