using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity2 : MonoBehaviour
{
    public static Gravity2 world;
    public float forceGravitation;
    public List<Rigidbody> objects;
    public Color colorGizmo;
    public float rangoDelCampo;
    void Awake()
    {
        Planets.singleton.Planetas.Add(this);
    }

    public void addObjets(Rigidbody rObj)
    {
        objects.Add(rObj);
    }
    // Start is called before the first frame update
   

    // Update is called once per frame
    void FixedUpdate()
    {
        foreach (Rigidbody objeto in objects)
        {
            if ((transform.position - objeto.transform.position).magnitude <= rangoDelCampo)
            { 
                Vector3 direccionG = (objeto.transform.position - transform.position).normalized;
                Vector3 direccionObj = objeto.transform.up;     
               objeto.AddForce(direccionG * forceGravitation * Time.fixedDeltaTime);
                Quaternion mRot = Quaternion.FromToRotation(direccionObj, direccionG) * objeto.transform.rotation;
                objeto.transform.rotation = Quaternion.Slerp(objeto.transform.rotation, mRot, 360 * Time.fixedDeltaTime);
            }
        }
    }


    void OnDrawGizmosSelected()
    {
        Gizmos.color = colorGizmo;
        Gizmos.DrawWireSphere(transform.position, rangoDelCampo);
        Color mColor = colorGizmo;
        mColor.a = 0.2f;
        Gizmos.color = mColor;
        Gizmos.DrawSphere(transform.position, rangoDelCampo);
    }
}
