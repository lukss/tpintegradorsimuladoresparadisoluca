using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlJugador : MonoBehaviour
{
    private float x, y;
    float vel=3;
    public GameObject Jugador;
    int cont;
    float rotVel = 100;
    public Rigidbody rb;
    public float salto;
    private bool Piso = true;
    public int maxSaltos = 2;
    public int saltoActual = 0;
    public float saltoPlaneta = 2000;
    public GameObject planeta2;
    public Animator animator;
    public GameObject espadaGolpe;
    public GameObject arana;
    int vida = 0;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        Rec();
        cont = 0;
    }

    void Rec()
    {
      
        if (cont >= 5)
        {

            planeta2.gameObject.SetActive(true);

        }
    }

    void cVida()
    {
        if (vida >= 3)
        {
            SceneManager.LoadScene("SampleScene");
        }
    }

    void pocaVida()
    {
        if(vida ==2)
        {
            vel = 1.5F;
        }
    }

    void pPlus()
    {
        vel = 4;
        vida = 0;

    }

 

    void Update()
    {
        if (x > 0 || x < 0 || y > 0 || y < 0)
        {
            animator.SetBool("Other", true);
        }

        movAdelanteAtras();
          movCostados();
         SaltarPlanetas();
        Espada();

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        if (Input.GetButtonDown("Jump") && (Piso || maxSaltos > saltoActual))
        {
            rb.velocity = new Vector3(0f, salto, 0f * Time.deltaTime);
            rb.AddForce(Vector3.up * salto, ForceMode.Impulse);
            animator.Play("Jump_start");
            animator.Play("Jump_loop");
            Piso = false;
            saltoActual++;
            espadaGolpe.gameObject.SetActive(false);

        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        }


    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Item") == true)
        {
            cont = cont + 1;
            Rec();
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("Win") == true)
        {
            SceneManager.LoadScene("Gane");
        }
        if (other.gameObject.CompareTag("Pinchos") == true)
        {

            vida = vida + 1;
            pocaVida();
            cVida();

       
        }

        if (other.gameObject.CompareTag("Plus") == true)
        {
            pPlus();
            other.gameObject.SetActive(false);
        }
    
    }

    void movAdelanteAtras()
    {
        x = Input.GetAxis("Horizontal");
       
        if (Input.GetKey(KeyCode.W))
        {

            espadaGolpe.gameObject.SetActive(false);
            transform.Translate(new Vector3(0, 0, vel * Time.deltaTime));
            animator.SetFloat("VelX", x);
        }

        if (Input.GetKey(KeyCode.S))
        {
            espadaGolpe.gameObject.SetActive(false);
            transform.Translate(new Vector3(0, 0, -vel * Time.deltaTime));
            animator.SetFloat("VelX", -x);
        }

        
       
    }
    void movCostados()
    {

        y = Input.GetAxis("Vertical");
        if (Input.GetKey(KeyCode.A))
            {
            espadaGolpe.gameObject.SetActive(false);
            transform.Rotate(0, -rotVel * Time.deltaTime, 0);
            animator.SetFloat("VelY", -y);
        }

            if (Input.GetKey(KeyCode.D))
            {
            espadaGolpe.gameObject.SetActive(false);
            transform.Rotate(0, rotVel * Time.deltaTime, 0);
            animator.SetFloat("VelY", y);
        }
      


    }
    void SaltarPlanetas()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {


            espadaGolpe.gameObject.SetActive(false);
            rb.AddForce(transform.up * saltoPlaneta* 1f);
            animator.Play("Jump_start");
            animator.Play("Jump_loop");
        }
    }

  void Espada()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (x > 0 || x < 0 || y > 0 || y < 0)
            {
                animator.SetBool("Other", false);
            }

            animator.Play("Flip");
            espadaGolpe.gameObject.SetActive(true);



        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        Piso = true;
        saltoActual = 0;
    }
}

